FROM nginx:latest

# Remove default Nginx website content
RUN rm -rf /usr/share/nginx/html/*

# Copy the entire directory structure from your local machine to the container
COPY website.tar.gz /usr/share/nginx/html/

WORKDIR /usr/share/nginx/html/

RUN tar -xzf website.tar.gz
RUN rm -rf website.tar.gz

# Set appropriate permissions
RUN chown -R nginx:nginx /usr/share/nginx/html
RUN chmod -R 755 /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]